# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('im', '0002_auto_20160222_1838'),
    ]

    operations = [
        migrations.RenameField(
            model_name='chatuser',
            old_name='GUID',
            new_name='guid',
        ),
    ]
