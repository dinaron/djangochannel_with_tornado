# coding: utf-8

try:
    import json
except ImportError:
    import simplejson as json

from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View, TemplateView
from django.core.urlresolvers import reverse

from .helpers import encoder_fabric
from .errors import ImApiResult


class BaseView(View):

    result = None
    
    def __init__(self, **kwargs):
        self.result = ImApiResult(200)
        super(BaseView, self).__init__(**kwargs)

    def render_to_json_response(self, context=None, **response_kwargs):
        response_kwargs['content_type'] = 'application/json'

        context = context or self.result
        if hasattr(context, 'status_code'):
            response_kwargs['status'] = context.status_code

        if isinstance(context, str):
            return HttpResponse(context, **response_kwargs)
        return HttpResponse(
            json.dumps(context,
                       cls=encoder_fabric(self.request)),
            **response_kwargs
        )

    def redirect(self, reverse_name, args=None, **kwargs):
        return HttpResponseRedirect(reverse(reverse_name, args=args), **kwargs)

    def redirect_to_url(self, url, **kwargs):
        return HttpResponseRedirect(url, **kwargs)


class BaseTemplateView(BaseView, TemplateView):
    pass

class IndexView(BaseTemplateView):
    template_name = 'index.html'
