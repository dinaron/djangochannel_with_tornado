import inspect

from django.contrib import admin
from django.db.models import Model

import models

for attr in dir(models):
    item = getattr(models, attr)

    if not hasattr(item, '__module__') or item.__module__ != models.__name__:
        continue

    if inspect.isclass(item) and issubclass(item, Model) and not item._meta.abstract:
        admin.site.register(item)
