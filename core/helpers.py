# coding: utf-8
import redis

from datetime import datetime, date
from django.core import serializers
from django.db.models import Model, QuerySet
from django.utils import dateformat

from core.errors import ImApiResult

try:
    import json
except ImportError:
    import simplejson as json


class FormEncoder(json.JSONEncoder):
    request = None

    def default(self, obj):
        if isinstance(obj, Model):
            result = serializers.serialize('json', [obj])
            return result.strip('[]')
        elif isinstance(obj, datetime):
            return obj.strftime('%d.%m.%Y')
        elif isinstance(obj, date):
            return obj.strftime('%d.%m.%Y')
        elif isinstance(obj, QuerySet):
            return list(obj)
        elif isinstance(obj, ImApiResult):
            return dict(obj.to_json())

        return json.JSONEncoder.default(self, obj)


def encoder_fabric(request):
    FormEncoder.request = request
    return FormEncoder


def send_message(message):
    """ Send message to redis
    """

    channel_id = str(message.channel_id)
    sender_id = str(message.sender_id)
    sender_name = message.sender.name

    r = redis.StrictRedis()

    if sender_name:
        r.publish("".join(["channel_", channel_id, "_messages"]), json.dumps({
            "date_create": dateformat.format(message.date_create, 'U'),
            "sender": sender_name,
            "channel": channel_id,
            "text": message.text,
        }))

    for key in ("total_messages", "".join(["from_", sender_id])):
        r.hincrby(
            "".join(["channel_", channel_id, "_messages"]), key, 1,
        )
