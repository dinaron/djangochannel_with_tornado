from __future__ import unicode_literals

import datetime
import json
import time
import urllib

import brukva
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.httpclient

from django.conf import settings
from django.utils.importlib import import_module

session_engine = import_module(settings.SESSION_ENGINE)

from im.models import Channel, ChatUser

c = brukva.Client()
c.connect()


class MainHandler(tornado.web.RequestHandler):

    def data_received(self, chunk):
        pass

    def get(self):
        self.set_header('Content-Type', 'text/plain')
        self.write('Connected successfully.')


class MessagesHandler(tornado.websocket.WebSocketHandler):

    user = None
    user_id = None
    sender_name = None
    channel = None
    channel_id = None

    def data_received(self, chunk):
        pass

    def check_origin(self, origin):
        return True

    def __init__(self, *args, **kwargs):
        super(MessagesHandler, self).__init__(*args, **kwargs)
        self.client = brukva.Client()
        self.client.connect()

    def open(self, channel_id, user_id=None):
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)
        session = session_engine.SessionStore(session_key)
        try:
            self.user_id = user_id or session["_auth_user_id"]
            self.user = ChatUser.objects.get(guid=self.user_id)
            self.sender_name = self.user.name
        except (KeyError, ChatUser.DoesNotExist):
            self.close()
            return
        if not Channel.objects.filter(
                guid=channel_id, members=self.user).exists():
            self.close()
            return

        self.channel = "".join(['channel_', channel_id, '_messages'])
        self.channel_id = channel_id
        self.client.subscribe(self.channel)
        self.client.listen(self.show_new_message)
        print('{0} connected!'.format(self.sender_name))

    def handle_request(self, response):
        print(response)

    def on_message(self, message):
        if not message:
            return
        if len(message) > 10000:
            return
        c.publish(self.channel, json.dumps({
            "date_create": int(time.time()),
            "sender": self.sender_name,
            "channel": self.channel_id,
            "text": message,
        }))
        http_client = tornado.httpclient.AsyncHTTPClient()
        request = tornado.httpclient.HTTPRequest(
            settings.SEND_MESSAGE_API_URL,
            headers={
                'X_ACCESS_TOKEN': settings.API_KEY
            },
            method="POST",
            body=urllib.urlencode({
                "sender": self.user_id,
                "channel": self.channel_id,
                "message": message.encode("utf-8"),
            })
        )
        http_client.fetch(request, self.handle_request)

    def show_new_message(self, result):
        self.write_message(str(result.body))

    def on_close(self):
        try:
            self.client.unsubscribe(self.channel)
        except AttributeError:
            pass

        def check():
            if self.client.connection.in_progress:
                tornado.ioloop.IOLoop.instance().add_timeout(
                    datetime.timedelta(0.00001),
                    check
                )
            else:
                self.client.disconnect()
        tornado.ioloop.IOLoop.instance().add_timeout(
            datetime.timedelta(0.00001),
            check
        )

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/(?P<channel_id>[A-z0-9-]+)&(?P<user_id>[A-z0-9-]+)/", MessagesHandler),
])
