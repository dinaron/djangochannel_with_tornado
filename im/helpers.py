# coding: utf-8
from __future__ import unicode_literals

import re
from HTMLParser import HTMLParser




def html_decode(s):
    """ Returns the ASCII decoded version of the given HTML string
    :param unicode s: input string
    :return unicode:
    """
    html_symbols = {
        "'": '&#39;',
        '"': '&quot;',
        '>': '&gt;',
        '<': '&lt;',
        '&': '&amp;',
        '-': '&dash;',
        ' ': '&nbsp;',
        '«': '&laquo;',
        '»': '&raquo;',
    }
    for k, v in html_symbols.items():
        s = s.replace(v, k)

    # Cut all unprocessed symbols
    s = s.replace(r'&#([\\d]){1,6};', '').replace(r'&([\\w]){1,6};', '')

    return s


def strip_nbsp(s):
    """ Remove all non-break spaces from string
    :param unicode s: input string
    :return unicode:
    """
    spaces = (
        '\u200b', '\u00a0', '\u0020',
    )
    for nb in spaces:
        s = s.replace(nb, ' ')

    return s.strip().replace(r'\s{2,}', ' ')


def strip_tags(s):
    """ Removes HTML tags from string
    :param unicode s: input string
    :return unicode:
    """
    return strip_nbsp(re.sub(r'<.*?>', ' ', html_decode(s)))


class MessageTextParser(HTMLParser):
    """ Parse text
    """
    stack = []  # Contains found tags
    _data = []  # Contains result data

    def __init__(self, bad_tags=None):
        self.reset()
        self.tags = set(['script', 'iframe'] + bad_tags or [])

        self.self_closing_tags = [
            'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'keygen', 'wbr',
            'area', 'link', 'menuitem', 'meta', 'param', 'source', 'track',
        ]

        # Create re for cutting events
        self.handlers_re = re.compile(
            r'\s?on\w+=(?P<q>[\'"])({0}.*?)(?P=q)'.format(''), re.I)

        HTMLParser.__init__(self)

    def reset(self):
        """ Reset containers
        """
        self.stack = []
        self._data = []
        HTMLParser.reset(self)

    @property
    def __recording(self):
        """ Is data correct to recording
        """
        return not set(self.tags) & set(self.stack)

    def escape_events(self, text):
        """ Cuts events
        """
        return self.handlers_re.sub('', text)

    def handle_starttag(self, tag, attrs):
        """ Handles start tag
        """
        self.stack.append(tag)
        if self.__recording:
            # Cut events
            tag_text = self.escape_events(self.get_starttag_text())
            self._data.append(tag_text)  # Save

    def handle_endtag(self, tag):
        """ Handles end tag
        """
        if self.__recording:
            if tag not in self.self_closing_tags:
                self._data.append('</{0}>'.format(tag))  # Save
        if tag in self.stack:
            # Close tag
            while self.stack:
                t = self.stack.pop()
                if t == tag:
                    break

    def handle_data(self, data):
        """ Handle data
        """
        if self.__recording:
            self._data.append(data)

    def handle_entityref(self, name):
        """ Handle entities
        """
        self._data.append('&{};'.format(name))

    @property
    def text(self):
        """ Returns processed data
        """
        return ''.join(self._data)
