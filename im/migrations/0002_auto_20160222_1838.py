# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('im', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='access_token',
            field=models.CharField(default=uuid.uuid4, max_length=36, verbose_name='\u0422\u043e\u043a\u0435\u043d \u0434\u043e\u0441\u0442\u0443\u043f\u0430'),
        ),
    ]
