# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('im', '0003_auto_20160223_1417'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatuser',
            name='date_deleted',
            field=models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0443\u0434\u0430\u043b\u0435\u043d\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chatuser',
            name='deleted',
            field=models.BooleanField(default=False, verbose_name='\u0423\u0434\u0430\u043b\u0451\u043d'),
            preserve_default=True,
        ),
    ]
