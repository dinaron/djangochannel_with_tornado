# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('owner', models.CharField(max_length=150, verbose_name='\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446')),
                ('owner_email', models.EmailField(max_length=150, verbose_name='\u0415-\u043c\u0430\u0438\u043b \u0432\u043b\u0430\u0434\u0435\u043b\u044c\u0446\u0430')),
                ('access_token', models.TextField(default=uuid.uuid4, max_length=36, verbose_name='\u0422\u043e\u043a\u0435\u043d \u0434\u043e\u0441\u0442\u0443\u043f\u0430')),
                ('is_accepted', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0441\u0442\u0443\u043f \u0440\u0430\u0437\u0440\u0435\u0448\u0451\u043d')),
                ('date_create', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438')),
            ],
            options={
                'verbose_name': '\u043f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=500, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('guid', models.CharField(default=uuid.uuid4, max_length=36, verbose_name='GUID')),
                ('date_create', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('deleted', models.BooleanField(default=False, verbose_name='\u0423\u0434\u0430\u043b\u0451\u043d')),
                ('date_deleted', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0443\u0434\u0430\u043b\u0435\u043d\u0438\u044f', blank=True)),
            ],
            options={
                'verbose_name': '\u043a\u0430\u043d\u0430\u043b',
                'verbose_name_plural': '\u041a\u0430\u043d\u0430\u043b\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChannelMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('read_only', models.BooleanField(default=False, verbose_name='\u0422\u043e\u043b\u044c\u043a\u043e \u0447\u0442\u0435\u043d\u0438\u0435')),
                ('channel', models.ForeignKey(verbose_name='\u041a\u0430\u043d\u0430\u043b', to='im.Channel')),
            ],
            options={
                'verbose_name': '\u0443\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0430 \u0447\u0430\u0442\u0430',
                'verbose_name_plural': '\u0423\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0438 \u0447\u0430\u0442\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChatUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=150, verbose_name='\u0418\u043c\u044f \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f')),
                ('GUID', models.CharField(default=uuid.uuid4, max_length=36, verbose_name='GUID')),
                ('is_accepted', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0441\u0442\u0443\u043f \u0440\u0430\u0437\u0440\u0435\u0448\u0451\u043d')),
                ('registration_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438')),
                ('application', models.ForeignKey(verbose_name='\u041f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0435', to='im.Application')),
            ],
            options={
                'verbose_name': '\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f',
                'verbose_name_plural': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField(default='', verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('guid', models.CharField(default=uuid.uuid4, max_length=36, verbose_name='GUID')),
                ('date_create', models.DateTimeField(default=datetime.datetime.now, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438')),
                ('channel', models.ForeignKey(verbose_name='\u041a\u0430\u043d\u0430\u043b', to='im.Channel')),
                ('sender', models.ForeignKey(verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c', to='im.ChatUser')),
            ],
            options={
                'verbose_name': '\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='channelmember',
            name='member',
            field=models.ForeignKey(verbose_name='\u0423\u0447\u0430\u0441\u0442\u043d\u0438\u043a', to='im.ChatUser'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channel',
            name='members',
            field=models.ManyToManyField(related_name='channel', to='im.ChatUser', through='im.ChannelMember', blank=True, null=True, verbose_name='\u0423\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channel',
            name='owner',
            field=models.ForeignKey(related_name='own_channel', verbose_name='\u0412\u043b\u0430\u0434\u0435\u043b\u0435\u0446', to='im.ChatUser'),
            preserve_default=True,
        ),
    ]
