

from django.conf.urls import patterns, url

from im.views import user as views


urlpatterns = patterns(
    '',
    url(r'^create/$', views.CreateUserView.as_view(), name='create'),
    url(r'^data/$', views.GetUserDataView.as_view(), name='data'),
    url(r'^list/$', views.GetUsersListView.as_view(), name='list'),

    url(r'^channels/$', views.UserChannelsListView.as_view(), name='channels'),

    url(r'^block/$', views.BlockUserView.as_view(), name='block'),
    url(r'^unblock/$', views.UnBlockUserView.as_view(), name='unblock'),
    url(r'^remove/$', views.DeleteUserView.as_view(), name='remove'),
    url(r'^repair/$', views.RepairUserView.as_view(), name='repair'),
)
