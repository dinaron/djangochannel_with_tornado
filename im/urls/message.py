# coding: utf-8
from django.conf.urls import patterns, url

from im.views import message as views




urlpatterns = patterns(
    '',
    url(r'^list$', views.GetLastMessagesListView.as_view(), name='list'),
    url(r'^send/$', views.SendMessageView.as_view(), name='send'),
    url(r'^remove/$', views.RemoveMessageView.as_view(), name='remove'),
    url(r'^repair/$', views.RepairMessageView.as_view(), name='repair'),
)
