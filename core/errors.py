# coding: utf-8

class ImApiResult(object):
    status_code = 200
    message = ''
    data = None

    def __init__(self, status_code, message='', data=None):
        self.status_code = status_code
        self.message = message
        self.data = data or {}

    def update(self, **kwargs):
        self.status_code = kwargs.get('status_code', 200)
        self.message = kwargs.get('message', '')
        self.data = kwargs.get('data', {})

    def error(self, message, status_code=500):
        self.update(status_code=status_code, message=message)

    def success(self, data=None, status_code=200):
        self.update(status_code=status_code, data=data or {})

    def to_json(self):
        return {'status_code': self.status_code, 'message': self.message,
                'data': self.data}
