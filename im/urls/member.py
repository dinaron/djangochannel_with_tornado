from django.conf.urls import patterns, url

from im.views import member as views




urlpatterns = patterns(
    '',
    url(r'^join/', views.JoinMemberView.as_view(), name='join'),
    url(r'^kick/', views.KickMemberView.as_view(), name='kick'),
    url(r'^mute/', views.MuteMemberView.as_view(), name='mute'),
    url(r'^unmute/', views.UnMuteMemberView.as_view(), name='unmute'),
)
