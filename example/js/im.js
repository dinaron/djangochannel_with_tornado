'use strict';

var ws;

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function setCookie(key, value) {
    document.cookie = escape(key) + '=' + escape(value);
}

function showMessage(message) {
    var message = message || {},
        chatList = $("div#channel_" + message['channel'] + " ul.message-list"),
        date = new Date(message['date_create'] * 1000),
        dateCreate = Array.join($.map([
            date.getHours(), date.getMinutes(), date.getSeconds()
        ], function(val, i) {
            return (val < 10) ? '0' + val : val;
        }), ':'),
        newMessage = '<li><span class="sender">' +
            message['sender'] + ', ' + dateCreate + ':' +
            '</span><br/><span class="message">' + message['text'] +
            '</span></li>';

    chatList.append(newMessage);
}

function start_chat_ws(channelId, userId) {
    ws = new WebSocket(socketUrl + channelId + '&' + userId + '/');

    ws.onmessage = function(event) {
        var message = JSON.parse(event.data);
        showMessage(message);
    };

    ws.onclose = function() {
        setTimeout(function() { start_chat_ws(channelId, userId) }, 5000);
    }
}

function loadMessages(channel_id) {
    $.ajax({
        url: chatUrl + 'message/list',
        method: "GET",
        headers: {
            HTTP_X_ACCESS_TOKEN: application
        },
        data: {
            channel: channel_id,
            limit: 10
        },
        crossDomain: true,
        dataType: "json"
    }).success(function(data, status) {
        if (!!data['status_code'] && data['status_code'] == 200) {
            var messages = data['data']['messages'] || [];
            messages = messages.reverse();
            messages.forEach(function(msg){
                showMessage(msg);
            });
        } else {
            alert('Не удалось загрузить сообщения канала: ' + data['message']);
        }
    }).error(function(e) {
        alert('Не удалось загрузить сообщения канала: ' + e);
    })
}

function initSettings(){
    var settings = $("div#settings"),
        userId = settings.find("#user").val(),
        channelId = settings.find("#channel").val(),
        sendButton = $("#send-message-button")
        ;

    if (!!userId && !!channelId) {
        setCookie("_auth_user_id", userId);

        loadMessages(channelId);

        $("div#channel_").attr('id', 'channel_' + channelId);

        if ("WebSocket" in window) {
            start_chat_ws(channelId, userId);
        } else {
            return false;
        }

        sendButton.prop('disabled', false);
        $(this).prop('disabled', true);
    } else {
        alert('Введите идентификаторы пользователя и канала!');
    }
}

function sendMessage() {
    var form = $("div#send-message"),
        textarea = form.find("textarea#message"),
        messageText = textarea.val()
        ;

    if (messageText == "") return false;

    if (ws.readyState != WebSocket.OPEN) return false;

    ws.send(messageText);

    textarea.val("");
}

$(function() {
    $("#send-message-button").prop('disabled', true);

    $("#settings-apply").prop('disabled', false).on('click', initSettings);
    $("#send-message-button").on('click', sendMessage);
});