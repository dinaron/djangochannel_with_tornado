from django.conf.urls import patterns, url, include



urlpatterns = patterns(
    '',
    url(r'^channel/', include('im.urls.channel', namespace='channel')),
    url(r'^user/', include('im.urls.user', namespace='user')),
    url(r'^member/', include('im.urls.member', namespace='member')),
    url(r'^message/', include('im.urls.message', namespace='message')),
)
