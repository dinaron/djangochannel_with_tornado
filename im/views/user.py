# coding: utf-8
from __future__ import unicode_literals

from django.utils.timezone import datetime

from core.views import BaseView
from im import models



USER_FIELDS = (
    'guid', 'registration_date', 'name', 'is_accepted',
    'deleted', 'date_deleted',
)


class CreateUserView(BaseView):
    """ Create new chat user
        POST: {'name': text -> str, ['accepted': bool -> str]}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        name = post.get('name')
        accepted = post.get('accepted', False)

        if not name:
            self.result.error('User name is not specified.')
        else:
            try:
                user = models.ChatUser.objects.create(
                    name=name, is_accepted=accepted,
                    application=request.application)
                self.result.success(
                    data=models.model_to_data(user, USER_FIELDS))
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class GetUserDataView(BaseView):
    """ Get chat user data
        GET: {'user': uuid -> str}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET
        user_id = _get.get('user')

        try:
            user = models.ChatUser.objects.get(guid=user_id)
        except models.ChatUser.DoesNotExist:
            user = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            self.result.success(data=models.model_to_data(user, USER_FIELDS))

        return self.render_to_json_response()


class GetUsersListView(BaseView):
    """ Get list of all application chat users
        GET: {['active': bool -> str | 'deleted': bool -> str |
               'accepted': bool -> str]}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET
        active = _get.get('active')
        if isinstance(active, (str, unicode)):
            active = active.lower() == 'true'

        deleted = _get.get('deleted')
        if isinstance(deleted, (str, unicode)):
            deleted = deleted.lower() == 'true'

        accepted = _get.get('accepted')
        if isinstance(accepted, (str, unicode)):
            accepted = accepted.lower() == 'true'

        _filter = {'application': request.application}
        if active is not None:
            _filter['deleted'] = not active

        if deleted is not None:
            _filter['deleted'] = deleted

        if accepted is not None:
            _filter['is_accepted'] = accepted

        try:
            users = models.ChatUser.objects.filter(**_filter)
        except Exception as e:
            self.result.error(str(e))
        else:
            self.result.success(data={'users': [
                models.model_to_data(x, USER_FIELDS) for x in users]})

        return self.render_to_json_response()


class BlockUserView(BaseView):
    """ Block chat user (make not accessible)
        POST: {'user': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')

        try:
            user = models.ChatUser.objects.get(
                guid=user_id, application=request.application)
        except models.ChatUser.DoesNotExist:
            user = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            user.is_accepted = False
            try:
                user.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class UnBlockUserView(BaseView):
    """ Unblock chat user (make accessible)
        POST: {'user': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')

        try:
            user = models.ChatUser.objects.get(
                guid=user_id, application=request.application)
        except models.ChatUser.DoesNotExist:
            user = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            user.is_accepted = True
            try:
                user.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class DeleteUserView(BaseView):
    """ Delete chat user
        POST: {'user': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')

        try:
            user = models.ChatUser.objects.get(
                guid=user_id, application=request.application)
        except models.ChatUser.DoesNotExist:
            user = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            user.deleted = True
            user.date_deleted = datetime.now()
            try:
                user.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class RepairUserView(BaseView):
    """ Repair chat user
        POST: {'user': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')

        try:
            user = models.ChatUser.objects.get(
                guid=user_id, application=request.application)
        except models.ChatUser.DoesNotExist:
            user = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            user.deleted = False
            user.date_deleted = None
            try:
                user.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class UserChannelsListView(BaseView):
    """ Get user channels list
        GET: {'user': uuid -> str, ['deleted': bool -> str]}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET
        user_id = _get.get('user')
        try:
            user = models.ChatUser.objects.get(guid=user_id)
        except models.ChatUser.DoesNotExist:
            user = None

        deleted = _get.get('deleted', None)
        if isinstance(deleted, (str, unicode)):
            deleted = deleted.lower() == 'true'

        if not user:
            self.result.error('User is not specified or is incorrect.')
        else:
            if deleted is None:
                _filter = {}
            else:
                _filter = {'deleted': deleted}

            _filter['members'] = user
            channels = models.Channel.objects.filter(**_filter)

            self.result.success(data={'channels': [{
                'channel': x.guid,
                'deleted': unicode(x.deleted),
                'date_deleted': unicode(x.date_deleted),
                'name': x.name,
                'date_create': unicode(x.date_create),
                'owner': x.owner.guid,
                'is_owner': unicode(x.owner == user),
            } for x in channels]})

        return self.render_to_json_response()
