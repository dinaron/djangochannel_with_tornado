# coding: utf-8
from __future__ import unicode_literals
from django.utils import dateformat

from django.utils.timezone import datetime
import logging

from core.views import BaseView
from core.helpers import send_message

from im import models




logger = logging.getLogger(__name__)


class GetLastMessagesListView(BaseView):
    """ Get channel last messages list
        GET: {}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET

        try:
            channel = models.Channel.objects.get(
                guid=_get.get('channel'), deleted=False)
        except models.Channel.DoesNotExist:
            channel = None

        deleted = _get.get('deleted')
        if isinstance(deleted, (str, unicode)):
            deleted = deleted.lower() == 'true'

        try:
            limit = int(_get.get('limit') or 0)
        except (TypeError, ValueError):
            limit = 0

        try:
            offset = int(_get.get('offset') or 0)
        except (TypeError, ValueError):
            offset = 0

        if not channel:
            self.result.error('Channel is not specified or is incorrect.')
        else:
            filter = {'channel': channel}
            if deleted is not None:
                filter['deleted'] = deleted
            else:
                filter['deleted'] = False
            messages = models.Message.objects.filter(**filter).order_by(
                '-date_create')
            if limit:
                messages = messages[offset:limit + offset]

            self.result.success({
                'messages': [{
                    'guid': x.guid,
                    'sender_id': x.sender.guid,
                    'sender': x.sender.name,
                    'date_create': dateformat.format(x.date_create, 'U'),
                    'text': x.text,
                    'deleted': x.deleted,
                    'date_deleted': x.date_deleted,
                    'channel': x.channel.guid,
                } for x in messages]
            })

        return self.render_to_json_response()


class SendMessageView(BaseView):
    """ Send new message in channel or to recipient (private channel)
        POST: {'sender': uuid -> str, 'message': str,
               ['channel': uuid -> str | 'recipient': uuid -> str]}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            sender = models.ChatUser.objects.get(
                guid=post.get('sender'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            sender = None

        try:
            channel = models.Channel.objects.get(
                guid=post.get('channel'), deleted=False)
        except models.Channel.DoesNotExist:
            channel = None

        message = post.get('message')

        try:
            recipient = models.ChatUser.objects.get(
                guid=post.get('recipient'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            recipient = None

        if not sender:
            self.result.error('Sender is not specified or is incorrect.')
        elif not channel and not recipient:
            self.result.error('Recipient or channel is required.')
        elif not message:
            self.result.error('Could not send an empty message.')
        else:
            is_private = False
            if not channel:
                # If channel is not specified, try to find the private channel
                # between the sender and the recipient
                channels = models.Channel.objects.filter(deleted=False)
                # Filter by first user
                channels = channels.filter(members=sender)
                # Filter by second user
                channels = channels.filter(members=recipient)
                try:
                    channel = channels.latest('id')
                    is_private = True
                except models.Channel.DoesNotExist:
                    channel = None

            if not channel:
                self.result.error('Could not find private channel. '
                                  'You must create one before.')
            elif sender != channel.owner:
                members = channel.channelmember_set.filter(member=sender)
                if not is_private:
                    # If it's not a private channel we must check
                    # sender's rights to send messages
                    members = members.filter(read_only=False)

                try:
                    sender = members.latest('id')
                except models.ChannelMember.DoesNotExist:
                    self.result.error('Sender can\'t talk in this channel.')

            if channel and sender:
                try:
                    new_message = models.Message.objects.create(
                        channel=channel, sender=sender, text=message)
                except Exception as e:
                    self.result.error(str(e))
                else:
                    self.result.success(data={
                        'sender': str(sender.guid),
                        'channel': str(channel.guid),
                        'text': new_message.text,
                        'guid': str(new_message.guid),
                        'date_create': str(new_message.date_create),
                    })
                    try:
                        send_message(new_message)
                    except Exception as e:
                        raise e  # FIXME: REMOVE
                        logger.error(
                            "Could not send message to redis: {0}".format(e))

        return self.render_to_json_response()


class RemoveMessageView(BaseView):
    """ Remove message by author or channel owner
        POST: {'user': uuid -> str, 'message': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(
                guid=post.get('user'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            message = models.Message.objects.get(guid=post.get('message'))
        except models.Message.DoesNotExist:
            message = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not message:
            self.result.error('Message is not specified or is incorrect.')
        elif message.sender != user or message.channel.owner != user:
            self.result.error('Permission denied: only channel owner or '
                              'message author can delete it.')
        else:
            message.deleted = True
            message.date_deleted = datetime.now()
            try:
                message.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class RepairMessageView(BaseView):
    """ Repair message by author or channel owner
        POST: {'user': uuid -> str, 'message': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(
                guid=post.get('user'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            message = models.Message.objects.get(guid=post.get('message'))
        except models.Message.DoesNotExist:
            message = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not message:
            self.result.error('Message is not specified or is incorrect.')
        elif message.sender != user or message.channel.owner != user:
            self.result.error('Permission denied: only channel owner or '
                              'message author can repair it.')
        else:
            message.deleted = False
            message.date_deleted = None
            try:
                message.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()
