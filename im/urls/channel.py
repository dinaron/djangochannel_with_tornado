

from django.conf.urls import patterns, url

from im.views import channel as views

urlpatterns = patterns(
    '',
    url(r'^create/$', views.CreateChannelView.as_view(), name='create'),
    url(r'^remove/$', views.RemoveChannelView.as_view(), name='remove'),
    url(r'^repair/$', views.RepairChannelView.as_view(), name='repair'),
    url(r'^members/$', views.GetChannelMembersView.as_view(), name='members'),
    url(r'^private/$', views.GetPrivateChannelView.as_view(), name='private'),
    url(r'^change_owner/$', views.ChangeChannelOwnerView.as_view(),
        name='change_owner'),
)
