# coding: utf-8
from django.conf import settings
from django.core.exceptions import PermissionDenied, MiddlewareNotUsed

from im.models import Application

EXCEPT_URLS = getattr(settings, 'AUTH_MIDDLEWARE_EXCEPT_URLS', ())


class ImApplicationAuthenticationMiddleware(object):
    """ Verify chat application
    """

    def process_request(self, request):
        """ Verify application and user
        """
        path = getattr(request, 'path', '')
        access_token = request.META.get('HTTP_X_ACCESS_TOKEN', None)

        if not any(path.startswith(x) for x in EXCEPT_URLS):
            app = Application.objects.filter(
                access_token=access_token, is_accepted=True)

            try:
                if not app.exists():
                    raise PermissionDenied('Your application is not allowed '
                                           'to work in KSTU-im-service.')
                else:
                    try:
                        request.application = app.latest('id')
                    except Application.DoesNotExist:
                        raise PermissionDenied('Your application is not allowed '
                                               'to work in KSTU-im-service.')
            except PermissionDenied as e:
                if not getattr(settings, 'DEBUG', False):
                    raise e
                else:
                    print('Request headers error: {0}'.format(e))
