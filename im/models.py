# coding: utf-8
from __future__ import unicode_literals

import uuid

from django.db import models
from django.utils.timezone import datetime

from im.helpers import strip_tags, MessageTextParser


UNALLOWED_TAGS = [
   'script', 'input', 'embed', 'style', 'iframe',
]


class Application(models.Model):
    """ Application chat-client
    """

    name = models.CharField('Наименование', max_length=100)
    owner = models.CharField('Владелец', max_length=150)
    owner_email = models.EmailField('Е-маил владельца', max_length=150)
    access_token = models.CharField('Токен доступа',
                                    max_length=36, default=uuid.uuid4)
    is_accepted = models.BooleanField('Доступ разрешён', default=False)
    date_create = models.DateTimeField('Дата и время регистрации',
                                       default=datetime.now)

    def __unicode__(self):
        return 'Приложение "{0}" ({1})'.format(self.name, self.owner)

    class Meta:
        verbose_name = 'приложение'
        verbose_name_plural = 'Приложения'


class ChatUser(models.Model):
    """ Application user, chat-member
    """

    name = models.CharField('Имя пользователя', max_length=150, default='')
    application = models.ForeignKey('Application', verbose_name='Приложение')
    guid = models.CharField('GUID', max_length=36, default=uuid.uuid4)
    is_accepted = models.BooleanField('Доступ разрешён', default=False)
    registration_date = models.DateTimeField('Дата и время регистрации',
                                             default=datetime.now)
    deleted = models.BooleanField('Удалён', default=False)
    date_deleted = models.DateTimeField('Дата и время удаления',
                                        null=True, blank=True)

    def __unicode__(self):
        return '{0} ["{1}"]'.format(
            self.name, getattr(self.application, 'name', ''))

    class Meta:
        verbose_name = 'пользователя'
        verbose_name_plural = 'Пользователи'


class Channel(models.Model):
    """ Chat channel
    """
    owner = models.ForeignKey('ChatUser', verbose_name='Владелец',
                              related_name='own_channel')
    name = models.CharField('Наименование', max_length=500, default='')
    guid = models.CharField('GUID', max_length=36, default=uuid.uuid4)
    date_create = models.DateTimeField('Дата и время создания',
                                       default=datetime.now)
    deleted = models.BooleanField('Удалён', default=False)
    date_deleted = models.DateTimeField('Дата и время удаления',
                                        null=True, blank=True)

    members = models.ManyToManyField(
        'ChatUser', verbose_name='Участники', null=True, blank=True,
        through='ChannelMember', through_fields=('channel', 'member'),
        related_name='channel')

    def __unicode__(self):
        return '"{0}" ({1})'.format(self.name, self.owner.name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Channel, self).save(force_insert, force_update, using,
                                  update_fields)
        # Owner must be a channel member
        if not self.members.exists():
            ChannelMember.objects.create(channel=self, member=self.owner)

    class Meta:
        verbose_name = 'канал'
        verbose_name_plural = 'Каналы'


class ChannelMember(models.Model):
    """ User chat-member
    """

    channel = models.ForeignKey('Channel', verbose_name='Канал')
    member = models.ForeignKey('ChatUser', verbose_name='Участник')
    read_only = models.BooleanField('Только чтение', default=False)

    def __unicode__(self):
        return '{0} в {1}'.format(self.member.name, self.channel)

    @property
    def muted(self):
        """ Is the chat member muted (owner can not be muted!)
        """
        return self.read_only and self.member != self.channel.owner
    
    class Meta:
        verbose_name = 'участника чата'
        verbose_name_plural = 'Участники чатов'


class Message(models.Model):
    """ ...
    """

    channel = models.ForeignKey('Channel', verbose_name='Канал')
    sender = models.ForeignKey('ChatUser', verbose_name='Отправитель')
    text = models.TextField('Текст', default='')
    guid = models.CharField('GUID', max_length=36, default=uuid.uuid4)
    date_create = models.DateTimeField('Дата отправки', default=datetime.now)
    deleted = models.BooleanField('Удалено', default=False)
    date_deleted = models.DateTimeField('Дата и время удаления',
                                        null=True, blank=True)

    def __unicode__(self):
        stripped = strip_tags(self.text or '')
        return stripped[:500] + ('...' if len(stripped) > 350 else '')

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        """ Before save we need to parse message for bad tags
        """
        message_parser = MessageTextParser(UNALLOWED_TAGS)
        message_parser.feed(self.text or '')
        message_parser.close()
        self.text = message_parser.text

        super(Message, self).save(
            force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'сообщение'
        verbose_name_plural = 'Сообщения'


def model_to_data(instance, fields):
    """ Convert model instance to ImApiResult-acceptable data (dict, actually)
    """
    return dict((x, unicode(getattr(instance, x, None))) for x in fields)
