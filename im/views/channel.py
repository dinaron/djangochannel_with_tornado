# coding: utf-8
from __future__ import  unicode_literals

from django.utils.timezone import datetime

from core.views import BaseView

from im import models

CHANNEL_FIELDS = (
    'name', 'guid', 'date_created', 'owner', 'deleted', 'date_deleted',
)


class CreateChannelView(BaseView):
    """ Create channel
        POST: {'owner': uuid -> str, 'name': text -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        owner_id = post.get('owner')
        name = post.get('name')

        try:
            owner = models.ChatUser.objects.get(guid=owner_id)
        except models.ChatUser.DoesNotExist:
            owner = None

        if not owner:
            self.result.error('Owner is not defined or is incorrect.')
        elif not name:
            self.result.error('Channel name is not specified.')
        else:
            channel = models.Channel.objects.create(owner=owner, name=name)
            self.result.success(data={'channel': models.model_to_data(
                channel, CHANNEL_FIELDS)})

        return self.render_to_json_response()


class RemoveChannelView(BaseView):
    """ Remove channel
        POST: {'user': uuid -> str, 'channel': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')
        channel_id = post.get('channel')

        try:
            channel = models.Channel.objects.get(guid=channel_id)
        except models.Channel.DoesNotExist:
            channel = None

        if not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif channel.owner.guid != user_id:
            self.result.error('User is not the owner of channel.')
        else:
            channel.deleted = True
            channel.date_deleted = datetime.now()
            try:
                channel.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class RepairChannelView(BaseView):
    """ Repair chanel
        POST: {'user': uuid -> str, 'channel': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        user_id = post.get('user')
        channel_id = post.get('channel')

        try:
            channel = models.Channel.objects.get(guid=channel_id)
        except models.Channel.DoesNotExist:
            channel = None

        if not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif channel.owner.guid != user_id:
            self.result.error('User is not the owner of channel.')
        else:
            channel.deleted = False
            channel.date_deleted = None
            try:
                channel.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class GetChannelMembersView(BaseView):
    """ Get channel members list
        GET: {'channel': uuid -> str, ['inactive': bool -> str]}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET
        channel_id = _get.get('channel')
        inactive = _get.get('inactive', False)

        try:
            channel = models.Channel.objects.get(guid=channel_id)
        except models.Channel.DoesNotExist:
            channel = None

        if not channel:
            self.result.error('Channel is not specified or is incorrect.')
        else:
            if inactive:
                members = channel.channelmember_set.all()
            else:
                members = channel.channelmember_set.filter(
                    member__is_accepted=True)

            members = [{
                'user': x.member.guid,
                'username': x.member.name,
                'accepted': x.member.is_accepted,
                'registration_date': x.member.registration_date,
                'read_only': x.muted,
            } for x in members]

            self.result.success(data={'members': members})

        return self.render_to_json_response()


class GetPrivateChannelView(BaseView):
    """ Get private channel for two users if exists
        GET: {'users': list of uuids -> str}
    """

    def get(self, request, *args, **kwargs):
        _get = request.GET
        users = _get.get('users') or ''
        users = users.split(',')

        users = models.ChatUser.objects.filter(guid__in=users)

        if len(users) != 2:
            self.result.error('Incorrect user list format.')
        else:
            channels = models.Channel.objects.filter(deleted=False)
            # Filter by first user
            channels = channels.filter(members=users[0])
            # Filter by second user
            channels = channels.filter(members=users[1])
            try:
                channel = channels.latest('id')
            except models.Channel.DoesNotExist:
                channel = None

            self.result.success(data={
                'channel': getattr(channel, 'guid', None),
            })

        return self.render_to_json_response()


class ChangeChannelOwnerView(BaseView):
    """ Change channel owner for new user
        POST: {'channel': uuid -> str,
               'old_owner': uuid -> str, 'news_owner': uuid -> list}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST
        channel_id = post.get('channel')
        old_owner_id = post.get('old_owner')
        new_owner_id = post.get('new_owner')

        try:
            channel = models.Channel.objects.get(guid=channel_id)
        except models.Channel.DoesNotExist:
            channel = None

        try:
            old_owner = models.ChatUser.objects.get(guid=old_owner_id)
        except models.ChatUser.DoesNotExist:
            old_owner = None

        try:
            # New owner must be accepted
            new_owner = models.ChatUser.objects.get(
                guid=new_owner_id, is_accepted=True)
        except models.ChatUser.DoesNotExist:
            new_owner = None

        if not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif not old_owner or old_owner != channel.owner:
            self.result.error('Current owner is incorrect.')
        elif not new_owner:
            self.result.error('New owner is not specified.')
        elif new_owner not in channel.members.all():
            self.result.error('New owner must be a channel member.')
        else:
            channel.owner = new_owner
            try:
                channel.save()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()
