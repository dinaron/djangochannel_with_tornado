# coding: utf-8
from __future__ import unicode_literals

from core.views import BaseView

from im import models




class JoinMemberView(BaseView):
    """ Join member to channel
        POST: {'user': uuid -> str, 'channel': uuid -> str,
               'member': uuid -> str, ['readonly': bool -> str]}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(guid=post.get('user'),
                                               is_accepted=True)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            channel = models.Channel.objects.get(guid=post.get('channel'))
        except models.Channel.DoesNotExist:
            channel = None

        try:
            member = models.ChatUser.objects.get(
                guid=post.get('member'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            member = None

        read_only = post.get('readonly', False)
        if isinstance(read_only, (str, unicode)):
            read_only = read_only.lower() == 'true'

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif not member:
            self.result.error('Member is not specified or is incorrect.')
        elif user != channel.owner:
            self.result.error('Permission denied.')
        elif member in channel.members.all():
            self.result.error('Member is already in this channel.')
        else:
            try:
                models.ChannelMember.objects.create(
                    member=member, channel=channel, read_only=read_only)
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class KickMemberView(BaseView):
    """ Kick member from channel
        POST: {'user': uuid -> str, 'channel': uuid -> str,
               'member': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(guid=post.get('user'),
                                               is_accepted=True)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            channel = models.Channel.objects.get(guid=post.get('channel'))
        except models.Channel.DoesNotExist:
            channel = None

        try:
            member = models.ChatUser.objects.get(
                guid=post.get('member'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            member = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif not member:
            self.result.error('Member is not specified or is incorrect.')

        elif user != channel.owner:
            self.result.error('Permission denied.')
        elif member not in channel.members.all():
            self.result.error('Member not in this channel yet.')
        else:
            try:
                models.ChannelMember.objects.filter(
                    member=member, channel=channel).delete()
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class MuteMemberView(BaseView):
    """ Mute channel member
        POST: {'user': uuid -> str, 'channel': uuid -> str,
               'member': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(guid=post.get('user'),
                                               is_accepted=True)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            channel = models.Channel.objects.get(guid=post.get('channel'))
        except models.Channel.DoesNotExist:
            channel = None

        try:
            member = models.ChatUser.objects.get(
                guid=post.get('member'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            member = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif not member:
            self.result.error('Member is not specified or is incorrect.')

        elif user != channel.owner:
            self.result.error('Permission denied.')
        elif member not in channel.members.all():
            self.result.error('Member not in this channel yet.')
        else:
            try:
                models.ChannelMember.objects.filter(
                    member=member, channel=channel).update(read_only=True)
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()


class UnMuteMemberView(BaseView):
    """ Unmute channel member
        POST: {'user': uuid -> str, 'channel': uuid -> str,
               'member': uuid -> str}
    """

    def post(self, request, *args, **kwargs):
        post = request.POST

        try:
            user = models.ChatUser.objects.get(guid=post.get('user'),
                                               is_accepted=True)
        except models.ChatUser.DoesNotExist:
            user = None

        try:
            channel = models.Channel.objects.get(guid=post.get('channel'))
        except models.Channel.DoesNotExist:
            channel = None

        try:
            member = models.ChatUser.objects.get(
                guid=post.get('member'), is_accepted=True, deleted=False)
        except models.ChatUser.DoesNotExist:
            member = None

        if not user:
            self.result.error('User is not specified or is incorrect.')
        elif not channel:
            self.result.error('Channel is not specified or is incorrect.')
        elif not member:
            self.result.error('Member is not specified or is incorrect.')

        elif user != channel.owner:
            self.result.error('Permission denied.')
        elif member not in channel.members.all():
            self.result.error('Member not in this channel yet.')
        else:
            try:
                models.ChannelMember.objects.filter(
                    member=member, channel=channel).update(read_only=False)
            except Exception as e:
                self.result.error(str(e))

        return self.render_to_json_response()
